var gulp  = require("gulp");
var del = require("del");

gulp.task('move', ['clean'], function() {
    gulp.src('./**/*.*', { base: 'staticFiles/' })
        .pipe(gulp.dest("public"));
});

gulp.task('clean', function(){
    del.sync(['public/**']);
});