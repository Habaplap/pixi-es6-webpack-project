import Stage from './../ui/Stage.js';

export default class Game {

    constructor(){

        this._renderer = null;
        this._stage = null;
        this._ticker = null;

        /**
         * example cube
         * remove this property and all
         * references when you start your game
         */
        this._cube = null;

        this.initializeRenderer();
        this.initializeStage();
        this.initializeTicker();
        this.addEventListener();

        this.start();

        this.resize();
    }

    initializeRenderer(){
        this._renderer = new PIXI.autoDetectRenderer(window.innerWidth, window.innerHeight, {antialias: true, transparent: false, resolution: window.devicePixelRatio});
        document.getElementById('mainApp').appendChild(this._renderer.view);
        this._renderer.autoResize = true;
    }

    initializeStage(){
        this._stage = new Stage();
    }

    initializeTicker(){
        this._ticker = PIXI.ticker.shared;
        this._ticker.minFPS = 30;
        this._ticker.autoStart = true;
    }

    addEventListener(){
        window.onresize = () => {this.resize();};
        this._ticker.add(this.update, this);
    }

    resize(event){
        this._renderer.resize( window.innerWidth, window.innerHeight );

        /**
         * example handle resize event on cube
         */

        this._cube.x = window.innerWidth * 0.1;
        this._cube.y = window.innerHeight * 0.1

        console.log( this._cube.y, window.innerHeight * 0.1, window.innerHeight);
    }

    update(){
        this._renderer.render(this._stage);
    }


    /**
     *
     * Start with your game here
     *
     */
    start(){
        /**
         * create example Cube
         */

        this._cube = new PIXI.Graphics();
        this._cube.beginFill( 0x333333);
        this._cube.drawRect(0, 0, 200, 200);
        this._cube.endFill();

        this._stage.addChild(this._cube);
    }
}