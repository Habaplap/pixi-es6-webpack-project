'use strict';

var webpack = require('webpack');
var path = require('path');

var plugins = [];
if(process.env.NODE_ENV === "production"){
    plugins.push(
        new webpack.optimize.UglifyJsPlugin({
            minimize: true,
            compress: {
                warnings: false
            },
            output: {comments:false}
        })
    );
}
plugins.push( new webpack.HotModuleReplacementPlugin());
plugins.push( new webpack.NoErrorsPlugin());

module.exports = {
    devtool: 'source-map',
    entry: [
        'pixi.js',
        './src/js/index.js',
        'webpack-dev-server/client?http://0.0.0.0:8200', // WebpackDevServer host and port
        'webpack/hot/only-dev-server'],
    output: {
        path: path.join(__dirname, 'public'),
        filename: 'bundle.js'
    },
    resolve: {
        extensions: ["", ".js"]
    },
    plugins: plugins,
    module: {
        postLoaders: [
            {
                loader: "transform/cacheable?brfs"
            }
        ],
        loaders: [
            /*{
                test: /\.json$/,
                include: path.join(__dirname, 'node_modules', 'pixi.js'),
                loader: 'json',
            },*/
            {
                test: /\.js$/,
                exclude: path.join(__dirname, 'node_modules'),
                loader: 'babel-loader',
                query: {
                    presets: ['es2015-loose','stage-0']
                }
            },
            {
                test: /\.(sass|scss)$/,
                loader: 'style!css!sass'
            },
            {
                test: /\.css$/,
                loader: 'style!css'
            },
            {
                test: /\.json$/,
                loader: 'json-loader'
            },
            {
                test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
                loader: "file"
            },
            {
                test: /\.(woff|woff2)$/,
                loader: "url?prefix=font/&limit=5000"
            },
            {
                test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
                loader: "url?limit=10000&mimetype=application/octet-stream"
            },
            {
                test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                loader: "url?limit=50000&mimetype=image/svg+xml"
            },
            {
                test: /\.gif/,
                loader: "url?limit=50000&minetype=image/gif"
            },
            {
                test: /\.jpg/,
                loader: "url?limit=50000&minetype=image/jpg"
            },
            {
                test: /\.png/,
                loader: "url?limit=50000&minetype=image/png"
            }
        ]
    },
    devServer: {
        contentBase: "./public",
        noInfo: true,
        hot: true,
        inline: true,
        port: 8200
    }
};